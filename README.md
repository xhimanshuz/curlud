![alt text](Data/githubPortfolio.png)
# curlud
Transfer and download files with the help of Curl Library and Qt integration.

## Currently Supported features
* Uploading

## TODO
* Directory Uploading Supported
* Recursively Uploading
* Downloading Supported
* Recursively Downloading
* Transfer speed
* Speed Graph Feature
* Logs Feature
* Pause, Resume
* Muti-Task support
* Cross-platform Testing
* Minor and Major Bugs fixing
* Update checking
